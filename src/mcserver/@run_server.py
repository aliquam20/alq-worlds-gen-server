#!/usr/bin/python3
from alq_server_jumpstarter import alq_server_jumpstarter
import common_configuration

server_name = "AlqGenerator"
server_port = 25565
server_dir = "."
server_jar = "./cache/patched_1.16.4.jar"
main_class = "org.bukkit.craftbukkit.Main"
server_memory_start = "512M"
server_memory_limit = "1024M"

common_configuration.configure(server_name, server_port)

alq_server_jumpstarter.start_server(server_name, server_dir, server_jar, main_class, server_memory_start, server_memory_limit)
