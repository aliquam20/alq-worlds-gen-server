#!/usr/bin/python3
from alq_server_jumpstarter import alq_server_jumpstarter as starter

import os


# This function should be called by inherited servers
def configure(server_name, server_port):

	starter.replace_in("server.properties", {
		"%MOTD%": f"Aliquam {server_name}",
		"%SERVER_PORT%": f"{server_port}"
	})

	starter.replace_in("paper.yml", {
		"%TIMINGS_SERVER_NAME%": f"Aliquam {server_name}",
	})

	plugins = []
	for plugin in plugins:
		plugin_path = os.path.join("plugins", plugin, "config.yml")

		starter.replace_in(plugin_path, {
			"%ALQ_DB_HOST%": starter.get_env("ALQ_MC_DB_HOST"),
			"%ALQ_DB_PORT%": starter.get_env("ALQ_MC_DB_PORT"),
			"%ALQ_DB_USER%": starter.get_env("ALQ_MC_DB_USER"),
			"%ALQ_DB_PASS%": starter.get_env("ALQ_MC_DB_PASS"),
			"%ALQ_DB_SCHEMA%": starter.get_env("ALQ_MC_DB_SCHEMA")
		})
